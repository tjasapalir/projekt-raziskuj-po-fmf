from tkinter import *
from bs4 import BeautifulSoup
import urllib.request
import string
from collections import defaultdict
import datetime
import time
import ast # to naložimo da lahko iz datotek dobimo urnik
import unicodedata


try:
    validFilenameChars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    
    cas=time.asctime()
    cas1=cas.split()
    now = datetime.datetime.now()
    cas2 = now.time()
    trenutniCas = cas2.hour
    trenutniDan = cas1[0]
    if trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
        print('Za čas vikenda ni podatka o tem.')
    if trenutniDan == 'Mon':
        gIndeks = 1
    if trenutniDan == 'Tue':
        gIndeks = 2
    if trenutniDan == 'Wed':
        gIndeks = 3
    if trenutniDan == 'Thu':
        gIndeks = 4
    if trenutniDan == 'Fri':
        gIndeks = 5
        
    casovniOkvir=''+ str(trenutniCas) + '-' + str(trenutniCas + 1)

    #sparsamo imenik vseh sodelavcev na fakulteti
    stran1 = urllib.request.urlopen('http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/Izbira2.htm')
    tekst = stran1.read()
     
    soup1 = BeautifulSoup(tekst)
    tekst1= soup1.find_all('option')
    seznam=[]
    for s in tekst1:
        seznam.append((s.getText()).strip())
    seznamVseh=seznam[:]

    # naredimo podobno še za smer/letnik
    stran2 = urllib.request.urlopen('http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/Izbira4.htm')
    tekst2 = stran2.read()
    soup2= BeautifulSoup(tekst2)
    tekst3= soup2.find_all('option')
    seznam2=[]
    for s in tekst3:
        seznam2.append((s.getText()).strip())
    seznamLetnik=seznam2[:]

    #naredimo podobno še za predavalnice
    stran3=urllib.request.urlopen('http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/Izbira1.htm')
    tekst4=stran3.read()
    soup3=BeautifulSoup(tekst4)
    tekst5=soup3.find_all('option')
    seznam3=[]
    for s in tekst5:
        staro=(s.getText()).strip()
        if '(' in staro:
            indeks = (s.getText()).index('(')
            novo = staro[:indeks-1].strip()
        else:
            novo=staro
        seznam3.append(novo)
    seznamPredavalnic=seznam3[:]


    def table_to_list(table):
        dct = table_to_2d_dict(table)
        return list(iter_2d_dict(dct))

    def table_to_2d_dict(table):
        
        result = defaultdict(lambda: defaultdict())
        rows = table.findAll('tr')
        for row_i, row in enumerate(rows):
            for col_i, col in enumerate(row.findAll('td')):
                colspan = int(col.get('colspan', 1))
                rowspan = int(col.get('rowspan', 1))
                col_data = col.get_text()
                

                while row_i in result and col_i in result[row_i]:
                    col_i += 1



                for i in range(row_i, row_i + rowspan):
                    for j in range(col_i, col_i + colspan):
                        if col_data != '  ':
                            result[i][j] = col_data
                        else:
                            result[i][j] = None
                            

        return result
    def iter_2d_dict(dct):
        for i, row in sorted(dct.items()):
            cols = []
            for j, col in sorted(row.items()):
                cols.append(col)
            yield cols

    def trenutnoProste():
        if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
            print('Fakulteta je ob tem času zaprta')
        elif trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
           return 'Za čas vikenda ni podatka o tem.'
        else:
            
            proste2='V drugem nadstropju so proste : '
            proste3 ='\n \n V tretjem nadstropju so proste : '
            proste4 = '\n \n V četrtem nadstropju so proste : '
            proste5 = '\n \n V petem nadstropju so proste : '
            proste6 = '\n \n Proste so še naslednje predavalnice : '
            #url = 'http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/urnikhtm.exe?Izbira=Ucilnica&ucilnica='
            for ucilnica in seznamPredavalnic:
                url = 'http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/urnikhtm.exe?Izbira=Ucilnica&ucilnica='
                url+=ucilnica
                urlRead = urllib.request.urlopen(url)
                html = urlRead.read()
                html1 = html.decode("ISO-8859-2")
                html2=html1.replace("</TR>","</TR><TR>")
                soup = BeautifulSoup(html2)
                table = soup.find('table')
                rows = table.findAll('tr')
                urnikProste = table_to_list(table)

                
                for i in urnikProste:
                    if casovniOkvir in i and i[gIndeks] == None:
                        if ucilnica in ['2.01', '2.02', '2.03', '2.04', '2.05']:
                            proste2 += ucilnica
                            proste2 += ' in '
                        elif ucilnica in ['3.04', '3.05', '3.06', '3.07', '3.10', '3.11', '3.12', '3.13', '303', '304', '306', '312', '317']:
                            proste3 += ucilnica
                            proste3 += ' in '
                        elif ucilnica in ['4.01', '4.02', '4.07', '4.12', '4.13', '4.20', '4.21', '4.22', '4.30', '4.31', '4.32', '4.33', '4.34']:
                            proste4 += ucilnica
                            proste4 += ' in '
                        elif ucilnica in ['5.01', '5.02', '5.03', '5.04', '5.07', '5.09', '5.11', '5.12', '5.14', '5.19', '5.20']:
                            proste5 += ucilnica
                            proste5 += ' in '
                        else:
                            proste6 += ucilnica
                            proste6 += ' in '
                        
            proste21 = proste2.strip(' in ')
            proste31 = proste3.strip(' in ')
            proste41 = proste4.strip(' in ')
            proste51 = proste5.strip(' in ')
            proste61 = proste6.strip(' in ')
            proste = proste21 + proste31 + proste41 + proste51 + proste61

            return proste

    def kjeNahajaLetnik():
        slovar = {}

        if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
            for letnik in seznamLetnik:
                slovar[letnik] = 'Fakulteta je ob tem času zaprta'
        elif trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
            for letnik in seznamLetnik:
                slovar[letnik] = 'Za čas vikenda ni podatka o tem.'

        else:
            for letnik in seznamLetnik:
                spremenjenLetnik = letnik.replace(' ','+').replace('č','%E8').replace('š','%9A').replace('Š','%8A')
                url = 'http://www-lp.fmf.uni-lj.si/urnik/urnik1011zimski/urnikhtm.exe?Izbira=Smer&smer='
                url += spremenjenLetnik
                urlRead = urllib.request.urlopen(url)
                html = urlRead.read()
                html1 = html.decode("ISO-8859-2")
                html2=html1.replace("</TR>","</TR><TR>")
                soup = BeautifulSoup(html2)
                table = soup.find('table')
                rows = table.findAll('tr')
                urnikLetnik = table_to_list(table)
            
                for i in urnikLetnik:
                    if casovniOkvir in i and i[gIndeks] != None:

                        slovar[letnik] = i[gIndeks]
                    if casovniOkvir in i and i[gIndeks] == None:

                        slovar[letnik] = 'Prosta ura'
        return slovar








    class ProgramFMF():

        def __init__(self,okno):
            self.platno = Canvas(okno)
            self.platno.pack(expand = True, fill = 'both')
            self.ozadje = PhotoImage(file="DSC_0470_27.gif")
            self.platno.img = self.ozadje
            self.platno.create_image(0, 0, anchor=NW, image=self.ozadje)
            gumbProste = Button(self.platno, text = "Proste predavalnice", \
                                font="Calibri",command=self.oknoProste,bd=0)
            self.platno.create_window(50,50, window = gumbProste, anchor = NW)
            gumbZapri = Button(self.platno, text = 'Izhod', font="Calibri", command = self.zapri, bd = 0)
            self.platno.create_window(600,600, window = gumbZapri, anchor = NW)
            var1 = StringVar(okno)
            opcijeLetnik = OptionMenu(self.platno, var1 ,*kjeNahajaLetnik())
            var1.set('1. letnik: finančna matematika')
            self.platno.create_window(10,150, window = opcijeLetnik, anchor = NW)

            self.platno.create_text(100,200,font=("Purisa",11), text = 'Izbrani letnik ima na urniku')
            napisLetnik = StringVar()
            slovar2 = {}
            imeLetnik = var1.get()
            letnikDat = str(unicodedata.normalize('NFKD', imeLetnik).encode('ASCII', 'ignore'))
            letnikDat1 = ''.join(c for c in letnikDat if c in validFilenameChars)
            
            imeDat1 = str(letnikDat1) + '.txt'
            potDat1 = 'C:\\Users\\Tjaša\\repos\\projekt-raziskuj-po-fmf\\' + imeDat1
            datoteka1 = open(potDat1,'r')
            prebrano1 = datoteka1.readlines()
            #prebrano1 = ast.literal_eval(prebrano1)
            prebrano1Ok =  eval(''.join(i for i in prebrano1))# to je zdaj letnikov urnik
            if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
                slovar2[imeLetnik] = 'Fakulteta je ob tem času zaprta'

            elif trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
                slovar2[imeLetnik] = 'Za čas vikenda ni podatka o tem.'

            else:
                for i in prebrano1Ok:
                    if casovniOkvir in i and i[gIndeks] != None:
                        slovar2[imeLetnik] = i[gIndeks]
                    if casovniOkvir in i and i[gIndeks] == None:
                        slovar2[imeLetnik] = 'Prosta ura'
            napisLetnik.set(slovar2[imeLetnik])
            #var1.trace('w',napisLetnik.set(kjeNahajaLetnik()[var1.get()]))
            letnik_ent = Entry(self.platno, font=("Purisa",11), text = napisLetnik, width = 50)
            self.platno.create_window(200,200, window = letnik_ent, anchor = NW)
            #var1.trace('w',napisLetnik.set(kjeNahajaLetnik()[var1.get()]))
            
            var2 = StringVar(okno)
            opcijePredavatelj = OptionMenu(self.platno, var2 ,*seznamVseh)
            var2.set('Bajec')
            self.platno.create_window(10,350, window = opcijePredavatelj, anchor = NW)

            self.platno.create_text(120,400,font=("Purisa",11), text = 'Izbrani predavatelj ima na urniku')
            napisPredavatelj = StringVar()
            slovar = {}
            imePred = var2.get()
            spremenjenPred = imePred.replace(' ','+').replace('č','%E8').replace('š','%9A').replace('Š','%8A').replace('ć','%E6').replace('ž','%9E').replace('Č','%C8').replace('Ž','%8E')
            imeDat = str(spremenjenPred) + '.txt'
            potDat = 'C:\\Users\\Tjaša\\repos\\projekt-raziskuj-po-fmf\\' + imeDat
            datoteka = open(potDat,'r')
            prebrano = datoteka.readlines()
            prebranoOk = eval(''.join(i for i in prebrano))# to je zdaj predavateljev urnik
            if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
                slovar[imePred] = 'Fakulteta je ob tem času zaprta'

            elif trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
                slovar[imePred] = 'Za čas vikenda ni podatka o tem.'

            else:
                for i in prebranoOk:
                    if casovniOkvir in i and i[gIndeks] != None:
                        slovar[imePred] = i[gIndeks]
                    if casovniOkvir in i and i[gIndeks] == None:
                        slovar[imePred] = 'Prosta ura'
            napisPredavatelj.set(slovar[imePred])
            #var2.trace('w',napisPredavatelj.set(self.kjeNahajaPredavatelj))
            predavatelj_ent = Entry(self.platno, font=("Purisa",11), text = napisPredavatelj, width = 50)
            self.platno.create_window(230,400, window = predavatelj_ent, anchor = NW)
            #var2.trace('w',napisPredavatelj.set(kjeNahajaPredavatelj()[var2.get()]))
            




        
        def zapri(self):
            gui.destroy()

        def spremeniVelikost(self,event):
            w,h = size = self.GetSize()
            if size != self.previous_size:
                #update font size
                self.previous_size = size

        def kjeNahajaPredavatelj(self):
            slovar = {}
            imePred = var2.get()
            spremenjenPred = imePred.replace(' ','+').replace('č','%E8').replace('š','%9A').replace('Š','%8A').replace('ć','%E6').replace('ž','%9E').replace('Č','%C8').replace('Ž','%8E')
            imeDat = str(spremenjenPred) + '.txt'
            datoteka = open(imeDat,'r')
            prebrano = datoteka.readlines()
            prebrano = ast.literal_eval(prebrano)# to je zdaj predavateljev urnik
            if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
                slovar[imePred] = 'Fakulteta je ob tem času zaprta'

            elif trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
                slovar[imePred] = 'Za čas vikenda ni podatka o tem.'

            else:
                for i in prebrano:
                    if casovniOkvir in i and i[gIndeks] != None:
                        slovar[imePred] = i[gIndeks]
                    if casovniOkvir in i and i[gIndeks] == None:
                        slovar[imePred] = 'Prosta ura'
            return slovar[imePred]
                
            
            

        


        

       
        def oknoProste(self):
            self.oknoProste = Tk()
            self.oknoProste.title('Proste predavalnice')
            self.oknoProste.geometry('882x661')
            self.napis = Label(self.oknoProste, text = str(trenutnoProste()))
            self.napis.pack()
            self.oknoProste.mainloop()


    gui = Tk()
    gui.title("Raziskuj po FMF")
    gui.geometry("882x661")
    program =ProgramFMF(gui)
    gui.mainloop()

except urllib.error.URLError:
        print('Za delovanje potrebuješ povezavo z internetom.')

except AttributeError:
        print('Verjetno je napaka pri dostopu do tabele urnika.')
