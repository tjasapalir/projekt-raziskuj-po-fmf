# to bodo naše pomožne funkcije
from bs4 import BeautifulSoup
import urllib.request
import string
from collections import defaultdict
import datetime
import time

cas=time.asctime()
cas1=cas.split()
now = datetime.datetime.now()
cas2 = now.time()
trenutniCas = cas2.hour
trenutniDan = cas1[0]
if trenutniDan not in ['Mon','Tue','Wed','Thu','Fri']:
    print('Za čas vikenda ni podatka o tem.')
if trenutniDan == 'Mon':
    gIndeks = 1
if trenutniDan == 'Tue':
    gIndeks = 2
if trenutniDan == 'Wed':
    gIndeks = 3
if trenutniDan == 'Thu':
    gIndeks = 4
if trenutniDan == 'Fri':
    gIndeks = 5
    
casovniOkvir=''+ str(trenutniCas) + '-' + str(trenutniCas + 1)

#sparsamo imenik vseh sodelavcev

stran = urllib.request.urlopen('http://www.fmf.uni-lj.si/si/imenik/vsi/')
tekst = stran.read().decode('utf8')
 
soup = BeautifulSoup(tekst)
tekst1= soup.find_all('a')
seznam=[]
for s in tekst1:
    seznam.append(s.getText())
seznamVseh=seznam[14:len(seznam)-6]
print(seznamVseh)


#naredimo podobno še za smer/letnik
stran2 = urllib.request.urlopen('http://www-lp.fmf.uni-lj.si/urnik/Izbira4.htm')
tekst2 = stran2.read()
soup2= BeautifulSoup(tekst2)
tekst3= soup2.find_all('option')
seznam2=[]
for s in tekst3:
    seznam2.append((s.getText()).strip())
seznamLetnik=seznam2[:]
print(seznamLetnik)


#naredimo podobno še za predavalnice
stran3=urllib.request.urlopen('http://www-lp.fmf.uni-lj.si/urnik/Izbira1.htm')
tekst4=stran3.read()
soup3=BeautifulSoup(tekst4)
tekst5=soup3.find_all('option')
seznam3=[]
for s in tekst5:
    staro=(s.getText()).strip()
    if '(' in staro:
        indeks = (s.getText()).index('(')
        novo = staro[:indeks-1].strip()
    else:
        novo=staro
    seznam3.append(novo)
seznamPredavalnic=seznam3[:]
print(seznamPredavalnic)


table = soup.find('table')
rows = table.findAll('tr')
def table_to_list(table):
    dct = table_to_2d_dict(table)
    return list(iter_2d_dict(dct))

def table_to_2d_dict(table):
    
    result = defaultdict(lambda: defaultdict())
    
    for row_i, row in enumerate(rows):
        for col_i, col in enumerate(row.findAll('td')):
            colspan = int(col.get('colspan', 1))
            rowspan = int(col.get('rowspan', 1))
            col_data = col.get_text()
            

            while row_i in result and col_i in result[row_i]:
                col_i += 1



            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    if col_data != '  ':
                        result[i][j] = col_data
                    else:
                        result[i][j] = None
                        

    return result
def iter_2d_dict(dct):
    for i, row in sorted(dct.items()):
        cols = []
        for j, col in sorted(row.items()):
            cols.append(col)
        yield cols

def trenutnoProste():
        if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
            print('Fakulteta je ob tem času zaprta')
        else:
            
            proste=''
            url = 'http://www-lp.fmf.uni-lj.si/urnik/urnikhtm.exe?Izbira=Ucilnica&ucilnica='
            for ucilnica in seznamPredavalnic:
                url+=ucilnica
                urlRead = urllib.request.urlopen(url)
                html = urlRead.read()
                html1 = html.decode("ISO-8859-2")
                html2=html1.replace("</TR>","</TR><TR>")
                soup = BeautifulSoup(html2)
                table = soup.find('table')
                rows = table.findAll('tr')
                urnikProste = table_to_list(table)
                for i in urnikProste:
                    for j in i:
                        if casovniOkvir in j and j[gIndeks] != None:
                            proste += ucilnica
                            proste += '  in  '
                        else:
                            break
            return proste



def kjeSeNahajaLetnik():
    if trenutniCas in [20,21,22,23,24,1,2,3,4,5,6]:
            print('Fakulteta je ob tem času zaprta')
    else:
        #zaenkrat je url naslov za primer
        url = 'http://www-lp.fmf.uni-lj.si/urnik/urnikhtm.exe?Izbira=Letnik&letnik=2.+letnik+(prakti%E8na+matematika)'
        kjeNahaja = 'Izbrani letnik se trenutno nahaja v '
        urlRead = urllib.request.urlopen(url)
        html = urlRead.read()
        html1 = html.decode("ISO-8859-2")
        html2=html1.replace("</TR>","</TR><TR>")
        soup = BeautifulSoup(html2)
        table = soup.find('table')
        rows = table.findAll('tr')
        urnikLetnik = table_to_list(table)
        for i in urnikLetnik:
            for j in i:
                if casovniOkvir in j and j[gIndeks] != None:
                    kjeNahaja += j[gIndeks]
                else:
                    break
        print(kjeNahaja)
        
        

